/* 
	Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/
let getAllListsOfBoard=require('./callback2');
let getAllCardsByListId=require('./callback3');

function getListsOfThanosBoardAndCardsOfMindListAndSpaceList(listsData,cardsData,boardId,MindListID,SpaceListID){
    if(cardsData==undefined||listsData==undefined ||listsData==null||cardsData==null ||typeof listsData!=='object'||SpaceListID== undefined
    ||typeof cardsData!=='object'|| listsData.length==0||cardsData.length==0|| MindListID== undefined||boardId== undefined){
        throw new Error('invalid arguments');
    }
    else{
        setTimeout(() => {
            // Your code here
           
            getAllListsOfBoard(listsData,boardId,(info)=>{
               console.log('All the lists for the Thanos board:','\n','\n',info,'\n')})
            getAllCardsByListId(cardsData,MindListID,(info)=>{
                console.log('All the cards for the Mind list:','\n','\n',info,'\n')})
            getAllCardsByListId(cardsData,SpaceListID,(info)=>{
                console.log('All the cards for the Space list:','\n','\n',info,'\n')})
         }, 2 * 1000);
    }
    
}

module.exports=getListsOfThanosBoardAndCardsOfMindListAndSpaceList;