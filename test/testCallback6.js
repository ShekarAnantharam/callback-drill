let getListsOfThanosBoardAndCardsOfAllLists=require("../callback6")

let listsData=require('../Data/lists.json');
let cardsData=require('../Data/cards.json');
let boardsData=require('../Data/boards.json');

let boardId=getThanosBoardId(boardsData);

// let MindListID=getListID(listsData,boardId,'Mind');
// let SpaceListID=getListID(listsData,boardId,'Space');
let listIDs=getListID(listsData,boardId)
let listNames=listsData[boardId].map(list=>{return list.name})

getListsOfThanosBoardAndCardsOfAllLists(listsData,cardsData,boardId,listIDs,listNames);

function getThanosBoardId(boardsData){
    let boardId=boardsData.filter(board=>{
        return board.name=='Thanos';
    });//console.log(boardId)
    return boardId[0].id;
}

function getListID(listsData,boardId){
    //console.log('this',listsData[boardId]  )
    let reqList=
    listsData[boardId].map(list=>{
        return list.id
    })
    
      return reqList;

}
