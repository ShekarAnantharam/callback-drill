let getListsOfThanosBoardAndCardsOfMindListAndSpaceList=require("../callback5")

let listsData=require('../Data/lists.json');
let cardsData=require('../Data/cards.json');
let boardsData=require('../Data/boards.json');

let boardId=getThanosBoardId(boardsData);

let MindListID=getMindListID(listsData,boardId,'Mind');
let SpaceListID=getMindListID(listsData,boardId,'Space');

getListsOfThanosBoardAndCardsOfMindListAndSpaceList(listsData,cardsData,boardId,MindListID,SpaceListID);

function getThanosBoardId(boardsData){
    let boardId=boardsData.filter(board=>{
        return board.name=='Thanos';
    });//console.log(boardId)
    return boardId[0].id;
}

function getMindListID(listsData,boardId,listName){
    //console.log('this',listsData[boardId]  )
    let reqList=listsData[boardId].filter(list=>{
        return list.name==listName;
    })
      return reqList[0].id;

}