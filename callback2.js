/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/
function getAllListsOfBoard(listsData,id,callback) {
    if(listsData==undefined ||typeof listsData!=='object'|| id== undefined){
        throw new Error('invalid arguments');
    }
    else{
        setTimeout(() => {
            // Your code here
            let lists=listsData[id];
            //console.log(id,'\n',lists)
            callback(lists);
        }, 2 * 1000);
    }
    
}

module.exports=getAllListsOfBoard;