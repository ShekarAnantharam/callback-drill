/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/
function getAllCardsByListId(cardsData,listId,callback) {
    if(cardsData==undefined||cardsData==null ||typeof cardsData!=='object'|| cardsData.length==0|| listId== undefined){
        throw new Error('invalid arguments');
    }
    else{
        setTimeout(() => {
            // Your code here
            let info=cardsData[listId];
            //console.log(cardsData)
            callback(info);
    
        }, 2 * 1000);
    }   
    
}
module.exports=getAllCardsByListId;