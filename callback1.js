/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/

//console.log(data)
function getBoardInfobyId(data,id,callback) {
    if(!Array.isArray(data) || data.length==0|| id== undefined){
        throw new Error('invalid arguments');
    }
    else{
        setTimeout(() => {
            // Your code here
            let info;
            info =data.filter(element=>{
                return element.id==id;
    
            })
            
            callback(info);
        
        }, 2 * 1000);
    }
    
}

//console.log(index)

module.exports=getBoardInfobyId;