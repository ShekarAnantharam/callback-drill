/* 
	Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

let getAllListsOfBoard=require('./callback2');
let getAllCardsByListId=require('./callback3');

function getListsOfThanosBoardAndCardsOfAllLists(listsData,cardsData,boardId,listIDs,listNames){
    if(cardsData==undefined||listsData==undefined ||listsData==null||cardsData==null ||typeof listsData!=='object'
    ||typeof cardsData!=='object'|| listsData.length==0||cardsData.length==0||!Array.isArray(listIDs)||!Array.isArray(listNames)||
    listNames.length==0|| listIDs.length==0){
        throw new Error('invalid arguments');
    }
    else{
        setTimeout(() => {
            // Your code here
           
            getAllListsOfBoard(listsData,boardId,(info)=>{
               console.log('All the lists for the Thanos board:','\n','\n',info,'\n')})
            listIDs.map((listID,index)=>{
                getAllCardsByListId(cardsData,listID,(info)=>{
                    if(info==undefined){
                        console.log('The given list ', listNames[index], 'does not contain any cards ');
                    }else{
                        console.log('All the cards for the list ' +listNames[index],':','\n','\n',info,'\n');
                    }
                })
                
                    
            })
            // getAllCardsByListId(cardsData,SpaceListID,(info)=>{
            //     console.log('All the cards for the Space list:','\n','\n',info,'\n')})
         }, 2 * 1000);
    }
    
}

module.exports=getListsOfThanosBoardAndCardsOfAllLists;
